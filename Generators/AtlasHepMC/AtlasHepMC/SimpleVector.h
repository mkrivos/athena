/* Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
   Author: Andrii Verbytskyi andrii.verbytskyi@mpp.mpg.de
*/
#ifndef ATLASHEPMC_SIMPLEVECTOR_H
#define ATLASHEPMC_SIMPLEVECTOR_H
#include "HepMC/SimpleVector.h"
#endif
